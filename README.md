# ModBot

ModBot - A Universal Bot Macro for MacroQuest

## Getting started

Please refer to the [**ModBot Wiki**](https://gitlab.com/EQWoobs/modbot/-/wikis/home) on this site for setup and usage information.


## Latest ModBot Code

The most recent version of ModBot code can be found in the source repository [**here**](https://gitlab.com/EQWoobs/modbot/-/tree/main/src).


## Prior Versions / Release Packages

The prior (and current) release packages can be fuond [**here**](https://gitlab.com/EQWoobs/modbot/-/releases).

